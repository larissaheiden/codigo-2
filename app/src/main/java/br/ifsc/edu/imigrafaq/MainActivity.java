package br.ifsc.edu.imigrafaq;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btn_Categoria1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_Categoria1 = (Button) findViewById(R.id.btnCategoria1);

        btn_Categoria1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                btn_Categoria1Activity();

            }
        });

    }

    private void btn_Categoria1Activity() {

        startActivity(new Intent(MainActivity.this, categoria1.class));

    }
}
