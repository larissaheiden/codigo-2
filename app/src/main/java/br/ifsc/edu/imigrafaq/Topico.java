package br.ifsc.edu.imigrafaq;

public class Topico {

    private String Pergunta;
    private String Resposta;


    public Topico(String Pergunta, String Resposta) {

        this.Pergunta = Pergunta;
        this.Resposta = Resposta;

    }

    public String getPergunta() {
        return Pergunta;
    }

    public String getResposta() {
        return Resposta;
    }

    public void setPergunta(String pergunta) {
        Pergunta = pergunta;
    }

    public void setResposta(String resposta) {
        Resposta = resposta;
    }
}